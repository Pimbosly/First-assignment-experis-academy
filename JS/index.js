/*
-------------- variables --------------
*/
const logInFieldElement = document.getElementById("username");
const logInButtonElement = document.getElementById("login");
const logInNameElement = document.getElementById("loginname");
const loginLabelElement = document.getElementById("loginlabel");
const passwordLabelElement = document.getElementById("passwordlabel");
const passwordElement = document.getElementById("password");
const moneyLabelElement = document.getElementById("moneylabel");
const loanButtonElement = document.getElementById("loan");
const loanLabelElement = document.getElementById("loanlabel");
const repayButtonElement = document.getElementById("repay");
const talkLabelElement = document.getElementById("talklabel");
const workButtonElement = document.getElementById("work");
const workLabelElement = document.getElementById("worklabel");
const bankButtonElement = document.getElementById("bank");
const workTextLabelElement = document.getElementById("worktext");

//users
const LOGGED_IN = "Log Out";
const LOGGED_OFF = "Log In";
const users = [];
let temp_user_position = "";

//computers
const computerSelectElement = document.getElementById("computers");
const computerImageElement = document.getElementById("computer-image");
const computerTitleElement = document.getElementById("computer-title");
const computerDescriptionElement = document.getElementById("computer-description");
const computerPriceElement = document.getElementById("computer-price");
const buyButtonElement = document.getElementById("buy");

const computers = [];
let displayed_computer = "";

//the given dataset of computers
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
.then((response) => {
    return response.json();
})
.then((computerResponse) => {
    computers.push(...computerResponse);
    renderComputers(computers);
})
/*
-------------- functions --------------
*/

//users

function logIn() {
  //check if you are logging in or off
  if (logInButtonElement.value == LOGGED_IN) {
    //you are trying to log off
    hideOrShow(true);
    talkLabelElement.innerText = "";
    temp_user_position = "";
  } else if (logInButtonElement.value == LOGGED_OFF) {
    //you are trying to log in
    const temp_username = logInFieldElement.value.trim();
    const temp_password = passwordElement.value.trim();
    //check if loginfield and passwordfield are not empty
    if (checkEmptyFields(temp_username, temp_password)) {
      talkLabelElement.innerText =
        "You forgot to fill in a username or password";
    } else {
      //check if the username and password are correct
      if (checkUserAndPassword(temp_username, temp_password)) {
        //password and name are correct
        logInNameElement.innerText = capitalizeFirstLetter(
          users[temp_user_position].username
        );
        fillMoneyLabel(0);
        fillLoanLabel(users[temp_user_position].loan);
        hideOrShow(false);
        talkLabelElement.innerText = "";
      } else {
        //password is incorrect
        talkLabelElement.innerText = "Your username or password is incorrect";
      }
    }
    //after the log in attempt, reset input fields
    logInFieldElement.value = "";
    passwordElement.value = "";
  } else {
    console.error("You are neither logged in or out");
    talkLabelElement.innerText = "This was not supposed to happen, hacker!";
  }
}

//refresh label and fill bank with money
function fillMoneyLabel(amount) {
  //JS sees values as strings and concatinate them
  const previousAmount = parseInt(users[temp_user_position].money);
  const addAmount = parseInt(amount);
  const bank = previousAmount + addAmount;
  moneyLabelElement.innerText = "Money: " + bank + ",-";
}

function fillLoanLabel(amount) {
  loanLabelElement.innerText = "You owe the bank: " + amount + ",-";
}

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function checkPassword(arrayposition, password) {
  //check if the password of the user on the given position is the same
  if (users[arrayposition].password === password) {
    //password is the same
    return true;
  } else {
    //password is different
    return false;
  }
}

//hide or show things depending if you are logging in or off
function hideOrShow(boolean) {
  if (boolean == true) {
    logInButtonElement.value = LOGGED_OFF;
    loginLabelElement.hidden = false;
    logInFieldElement.hidden = false;
    passwordElement.hidden = false;
    passwordLabelElement.hidden = false;
    loanButtonElement.hidden = true;
    logInNameElement.hidden = true;
    moneyLabelElement.hidden = true;
    loanLabelElement.hidden = true;
    repayButtonElement.hidden = true;
    workButtonElement.hidden = true;
    workLabelElement.hidden = true;
    bankButtonElement.hidden = true;
    workTextLabelElement.hidden = true;
  } else {
    logInButtonElement.value = LOGGED_IN;
    loginLabelElement.hidden = true;
    logInFieldElement.hidden = true;
    passwordElement.hidden = true;
    passwordLabelElement.hidden = true;
    loanButtonElement.hidden = false;
    logInNameElement.hidden = false;
    moneyLabelElement.hidden = false;
    workButtonElement.hidden = false;
    workLabelElement.hidden = false;
    bankButtonElement.hidden = false;
    workTextLabelElement.hidden = false;
    revealHideLoan();
  }
}

//hide or reveal the loan element
function revealHideLoan() {
  if (users[temp_user_position].loan > 0) {
    loanLabelElement.hidden = false;
    repayButtonElement.hidden = false;
  } else {
    loanLabelElement.hidden = true;
    repayButtonElement.hidden = true;
  }
}

//constructor function for User
function User(username, password, money, loan, work) {
  this.username = username;
  this.password = password;
  this.money = money;
  this.loan = loan;
  this.work = work;
}

//add a new user to the user array
function addUser(username, password, money) {
  //check if name and password are filled in
  if (checkEmptyFields) {
    //check if the username is not already taken
    if (checkExistingUser(username)) {
      //username is taken
    } else {
      //add the user to the array
      const newUser = new User(username, password, money, 0, 0);
      users.push(newUser);
    }
  } else {
    //name and or password is not filled in
  }
}

//uses both check functions to find out if you are allowed to log in
function checkUserAndPassword(username, password) {
  if (checkExistingUser(username)) {
    if (checkPassword(temp_user_position, password)) {
      return true;
    }
  }
  return false;
}

//check the user array if the username does not exist already
function checkExistingUser(username) {
  let counter = 0;
  for (const user of users) {
    //use a counter to save the position of the user in the array
    if (user.username === username) {
      //return true if username already exists
      temp_user_position = counter;
      return true;
    }
    counter++;
  }
  //if no same username is found, return false
  return false;
}

//check if the name and password are filled in
function checkEmptyFields(username, password) {
  if (username === "" || password === "") {
    //username or password is not filled in
    return true;
  } else {
    //username and password are filled in
    return false;
  }
}

//get a loan integer if you do not have one already. amount may not be negative or double the total owned value
function askLoanAmountPopUp() {
  const loan = users[temp_user_position].loan;
  let text;
  if (loan === 0) {
    const loanPrompt = prompt("Please enter the amount for your loan", "0");
    if (loanPrompt === null || loanPrompt === "") {
      text = "User cancelled the prompt.";
    } else {
      const loanNumber = parseInt(loanPrompt);
      if (Number.isInteger(loanNumber)) {
        if (loanNumber > users[temp_user_position].money * 2) {
          text =
            "User tried to get a loan bigger than double their own total value";
        } else if (loanNumber < 0) {
          text = "User tried to get a negative loan";
        } else {
          text = "User added  " + loanNumber + " to their bank";
          fillMoneyLabel(loanNumber);
          fillLoanLabel(loanNumber);
          users[temp_user_position].loan = loanNumber;
          users[temp_user_position].money += loanNumber;
          revealHideLoan();
        }
      } else {
        text = "User did not specify a correct number";
      }
    }
  } else {
    text = "User already has a pending loan. Pay it off first!";
  }
  talkLabelElement.innerText = text;
}

function updateWorkLabel() {
  workLabelElement.innerText =
    "Pay: " + users[temp_user_position].work + " money";
}

//add 100 currency to work
function work() {
  users[temp_user_position].work += 100;
  updateWorkLabel();
  talkLabelElement.innerText = "User added 100 to his work";
}

//add the work money to the total owned value. 10% goes to loans if u own one.
function bank() {
  if (users[temp_user_position].loan === 0) {
    fillMoneyLabel(users[temp_user_position].work);
    users[temp_user_position].money += users[temp_user_position].work;
    talkLabelElement.innerText = "User added " + users[temp_user_position].work + " to their bank";
  } else {
    let temp_work_percentage = users[temp_user_position].work * 0.1;
    let temp_work = users[temp_user_position].work - temp_work_percentage;
    fillMoneyLabel(temp_work);
    users[temp_user_position].money += temp_work;
    repayFunction(temp_work_percentage);
    talkLabelElement.innerText = "User added " + temp_work + " to their bank and used 10% to pay their loans";
  }
  users[temp_user_position].work = 0;
  updateWorkLabel();
}

//repay loans with your work money
function repayFunction(money){
    if(users[temp_user_position].loan !== 0)
    {
        //if you have more money than loans, you get rid of all loans and the remainder will go to bank
        if(money > users[temp_user_position].loan)
        {
            let temp_remainder = money - users[temp_user_position].loan;
            users[temp_user_position].loan = 0;
            fillMoneyLabel(temp_remainder);
            users[temp_user_position].money += temp_remainder;
            talkLabelElement.innerText = "User paid off their loan and added " + temp_remainder + " to their bank";
        }
        //if more loans than money, you put all money in loans
        else
        {
            users[temp_user_position].loan -= money;
            talkLabelElement.innerText = "User used " + money + " to pay off some loans";
        }
        users[temp_user_position].work = 0;
        fillLoanLabel(users[temp_user_position].loan);
        updateWorkLabel();

    }
    else{
        talkLabelElement.innerText = "You do not have an loans";
    }
}

function repay(){
    repayFunction(users[temp_user_position].work);
}

//computer

function renderComputers(computers){
    for (const computer of computers){
        computerSelectElement.innerHTML +=
        `<option value=${computer.id}>${computer.title}</option>`
    }
}

//render some elements
function renderSelectedComputer(computer)
{
    buyButtonElement.hidden = false;
    computerImageElement.hidden = false;
    computerTitleElement.innerText = computer.title;
    computerImageElement.src = computer.image;
    computerDescriptionElement.innerText = computer.description;
    computerPriceElement.innerText = "Price: " + computer.price;
}

/**@type HTMLSelectElement */
function onSelectChange() {
    const computerId = this.value;
    const computer = computers.find(computer => computer.id == computerId);
    renderSelectedComputer(computer);
    displayed_computer = computer;
}

//buy remove money from bank equal to computer price if you are logged in
function buy(){
    if(temp_user_position === "")
    {
        talkLabelElement.innerText = "You have to be logged in to buy something!";
    }
    else{
        if(users[temp_user_position].money < displayed_computer.price)
        {
            talkLabelElement.innerText = "You do not own enough money";
        }
        else{
            fillMoneyLabel(-displayed_computer.price);
            users[temp_user_position].money -= displayed_computer.price;
            talkLabelElement.innerText = "User bought a " + displayed_computer.title + " for " + displayed_computer.price;
        }
    }
}

/*
-------------- event handlers --------------
*/
logInButtonElement.addEventListener("click", logIn);
loanButtonElement.addEventListener("click", askLoanAmountPopUp);
workButtonElement.addEventListener("click", work);
bankButtonElement.addEventListener("click", bank);
repayButtonElement.addEventListener("click", repay);
buyButtonElement.addEventListener("click", buy);

computerSelectElement.addEventListener("change", onSelectChange);

//just for user friendliness I added an event listener for the enter key on the textfields for username and password
logInFieldElement.addEventListener("keyup", ({key}) => {
    if (key === "Enter") {
        logIn();
    }
})
passwordElement.addEventListener("keyup", ({key}) => {
    if (key === "Enter") {
        logIn();
    }
})

/*
-------------- other stuff --------------
*/
hideOrShow(true);
addUser("pim", "password", 1000);
addUser("poor", "food", 50);
addUser("rich", "money", 20000);
